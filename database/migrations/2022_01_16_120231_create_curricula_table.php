<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurriculaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curricula', function (Blueprint $table) {
            /**Esta tabla registra los curriculos creados por los usuarios */
            $table->id();
            $table->timestamps();
            $table->string('skill1', 30)->nullable();
            $table->string('skill2', 30)->nullable();
            $table->string('skill3', 30)->nullable();
            $table->string('skill4', 30)->nullable();
            $table->string('skill5', 30)->nullable();
            $table->string('skill6', 30)->nullable();

            $table->string('imagen')->nullable();

            $table->string('nombre', 25)->nullable();
            $table->string('apellido', 25)->nullable();
            $table->string('telefono', 50)->nullable();
            $table->string('titulo', 50)->nullable();
            $table->string('correo', 50)->nullable();
            $table->string('ubicacion', 50)->nullable();
            $table->string('contenido', 5000)->nullable();

            $table->string('tecnologia1', 30)->nullable();
            $table->string('tecnologia2', 30)->nullable();
            $table->string('tecnologia3', 30)->nullable();
            $table->string('tecnologia4', 30)->nullable();
            $table->string('tecnologia5', 30)->nullable();
            $table->string('tecnologia6', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curricula');
    }
}
