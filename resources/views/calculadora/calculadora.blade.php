@extends('layouts.app')

@section('titulo', 'Calculadorav1')

@section('content')


    @php
        
    @endphp

    <form action="calculadorav1" method="post" id="formulario">
        @csrf

        <label for="" class="blanco">Valor 1</label>

        <br>
        <br>

        <input type="number" id="val1" name="val1" autocomplete="off" autofocus onkeypress="return soloNumeros(event)" value="@if(isset($val1)){{$val1}}@endif">

        <br>
        <br>

        <label for="" class="blanco">Valor 2</label>

        <br>
        <br>

        <input type="number" id="val2" name="val2" autocomplete="off" onkeypress="return soloNumeros(event)" value="@if(isset($val2)){{$val2}}@endif">

        <br>
        <br>

        <select name="opcion" id="opcion">
            @foreach ($operaciones as $operacion => $text)
                <option value="{{ $operacion }}">{{ $text }}</option>
            @endforeach
        </select>

        <br>
        <br>

        <button type="submit">Calcular</button>
    
    </form>

    @if (isset($resultado))
        <p class="verdana blanco">resultado: {{ $resultado }}</p>
    @endif

    @if ($errors->any())
        <ul class="blanco verdana" >
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif



    <div id="app">
        <piea-component></piea-component>
    </div>

    <script src="{{asset('js/calculadora/calcular.js')}}"></script>


@endsection
