@extends('layouts.app')

@section('titulo', 'UX')

@section('content')

    <div class="UI">

        <br>

        <h1 class="h">Diseño UX</h1>


        <div class="p">
            
            <p>El UX está determinado por lo fácil o difícil que es para el usuario interactuar con los elementos de la interfaz de usuario que los diseñadores UI han creado.</p>
            <p>UX es la manera en que el usuario percibe, siente o interactúa con un sistema o un servicio. Se trata de la sensación del usuario cuando está en contacto e interactúa con una web, una app o un sistema. Todo lo que percibe al de comprar un producto online, navegar por una página web o moverse por las opciones de una interfaz digital, por ejemplo, son ejemplos de experiencia de usuario. Pero UX no es solo usabilidad, una pantalla puede ser usable, pero no generar buena experiencia de usuario.</p>
            <p>No se trata de una materia única centrada exclusivamente en el aspecto visual de una web o una app, sino que engloba muchos otros oficios que son necesarios para lograr un UX exitoso: interacción, arquitectura de la información, animación en diseño, estilo de comunicación... Este conjunto de puntos de vista tiene como resultado un producto mucho más sólido y con el que el usuario interactuará de manera más fluida. Un producto de UX debe tener look (ser atractivo visualmente), feel (el usuario debe sentirse cómodo a la hora de interactuar) y usabilidad (debe existir un propósito).</p>
            <p>No se trata de algo estático, un buen desarrollo de UX involucra una serie de fases para garantizar la calidad de un producto. Es necesario conocer las necesidades del cliente o de la marca, debe hacerse un user research para investigar las audiencias a las que va dirigido el producto y conocer sus necesidades, comportamientos y conducta.</p>
            <p></p>


            <br><br>
            <img src="{{ asset('img/ui-ux.jpg') }}" alt="" class="uiImg">


        </div>

        <br><br>



    </div>
    <div id="app">
        <pie-component></pie-component>
    </div>



@endsection