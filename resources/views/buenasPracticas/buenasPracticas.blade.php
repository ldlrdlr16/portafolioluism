@extends('layouts.app')

@section('titulo', 'Buenas Practicas')

@section('content')


    <div class="UI">

        <br>

        <h1 class="h">Buenas Practicas</h1>

        <div class="p">

            <h3>1. Tener un sistema de organización.</h3>
            <p> Se deben tener organizadas las carpetas y todo el proyecto de manera que sea facil distinguir cada archivo o cada parte del proyecto en secciones.</p>
            <h3> 2. Comenta tu código… moderadamente.</h3>
            <p> Comentar el codigo para decir que estas haciendo en cada parte del codigo y asi otros programadores puedan entender que hace.</p>
            <h3> 3. Claro, corto y conciso.</h3>
            <p> Se debe programar de manera clara y sencilla, sin redondeos ni duplicados de codigos.</p>
            <h3> 4. Lee código fuente.</h3>
            <p> Leer codigo de otros te hace mas sabio.</p>
            <h3> 5. Prueba tu código.</h3>
            <p> Testear el codigo sirve para ver si esta teniendo errores o no esta funcionando como se espera.</p>
            <h3> 6. Lleva un control de versiones.</h3>
            <p> Llevar un control de versiones de tu proyecto hace que puedas volver atras si algo falla.</p>
            <br><br>

        </div>

        <br><br>

    </div>   

    <div id="app">
        <pie-component></pie-component>
    </div>

@endsection