@extends('layouts.app')

    @section('content')

    <div id="app"></div>

    <h1 class="titulo">
        <a href="/" class="blanco">Portafolio</a>
    </h1>


    <div class="divs">
        <div class="div1"> <a href="calculadorav1" class="margin-auto_content" style="color:white;">Calculadora v1.0</a> </div>
        <div class="div2"> <a href="UI" class="margin-auto_content" style="color:black;">UI</a> </div>
        <div class="div1"> <a href="calculadorav2" class="margin-auto_content" style="color:white;">Calculadora v2.0</a> </div>
        <div class="div2"> <a href="UX" class="margin-auto_content" style="color:black;">UX</a></div>
        <br style="clear: left;" />
    </div>


    <div class="divs">
        <div class="div2"> <a href="/curriculum " class="margin-auto_content" style="color:black;">Curriculum</a> </div>
        <div class="div1"> <a href="protegido" class="margin-auto_content" style="color:white;">Protegido</a> </div>
        <div class="div2"> <a href="/blog" class="margin-auto_content" style="color:black;">Blog</a> </div>
        <div class="div1"> <a href="posts" class="margin-auto_content" style="color:white;">Investigaciones</a></div>
        <br style="clear: left;" />
    </div>


    <div class="divs">
        <div class="div1"> <a href="/POO" class="margin-auto_content" style="color:white;">POO</a> </div>
        <div class="div2"> <a href="/buenasPracticas" class="margin-auto_content" style="color:black;">Buenas Practicas</a> </div>
        <div class="div1"> <a href="/algoritmos" class="margin-auto_content" style="color:white;">Algoritmos</a> </div>
        <div class="div2"> <a href="#" class="margin-auto_content" style="color:black;"></a></div>
        <br style="clear: left;" />
    </div>

    <br>
    
    <p class="pie blanco made">Luis Molina Portafolio 2021</p>

    @endsection

