@extends('layouts.app')

@section('titulo', 'Blog')

@section('content')




    <div class="tituloBlog">
    <h2>
       <a href="/blog" class="blanco">Blog</a>->Registrar
    </h2>
    </div>
 

            <br>


                <form method="post" action="/blog/{{ $post->id }}" id="registrar" class="formularioRegisPost" >
                    @method('PATCH')
                    @csrf
   
                
                            <h2 class="blanco">Titulo</h2>
                            <input type="text" id="titulo" name="titulo" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,67)" onkeydown="return limitar(event,this.value,67)" class="inputt" autofocus autocomplete="off" value="{{$post->titulo}}"/>
                
                            <br>
                            <h2 class="blanco">Contenido</h2>
                            <textarea  id="contenido" class="verdana" name="contenido" cols="110" rows="50" onkeypress="return noSimbolosContenido(event)" onkeyup="return limitar(event,this.value,5000)" onkeydown="return limitar(event,this.value,5000)" class="inputt" autocomplete="off">{{$post->contenido}}</textarea>
                
                            <br>
                            <button type="submit" name="registrar" value="registrar">Confirmar</button>
                            <br>
                    </form>





    <div id="app">
        <pie-component></pie-component>
    </div>

    <script src="{{asset('js/posteo/val_regi.js')}}"></script>


@endsection


