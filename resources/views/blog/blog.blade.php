@extends('layouts.app')

@section('titulo', 'Blog')

@section('content')




    <div class="tituloBlog">
    <h2>Blog</h2>
    </div>
 
    <div class="row">
 
        
        <div class="columnaIzquierda">
            @foreach ($posts as $post)
            <a href="/blog/{{$post->id}}">
                <div class="tarjeta">
                    <h2>{{$post->titulo}}</h2>
                    <h4>Autor: {{$post->user['name']}}</h4>
                    <h5>{{$post->created_at}}</h5>
                    <p class="recortarTexto" style="width: 800px">{{$post->contenido}}</p>
                </div>
            </a>
            @endforeach
        </div>

 
        <div class="columnaDerecha">
                <a href="/blog/create" class="tarjeta">
                    <h2 class="center blanco">Redactar un post</h2>
                </a>
                <a href="/lista" class="tarjeta">
                    <h2 class="center blanco">Lista de posts</h2>
                </a>
            <div class="tarjeta">
                <h2>Sobre mi</h2>
                <p>Hola me llamo luis, tengo 22 años, estudio informatica en el IUJO y mi pais natal es Venezuela</p>
            </div>

        </div>

  
    </div>

    <div id="app">
        <pie-component></pie-component>
    </div>




@endsection

