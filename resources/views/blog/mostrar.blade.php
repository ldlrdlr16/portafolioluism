@extends('layouts.app')

@section('titulo', 'Blog')

@section('content')


<div class="mostrar">

  <br>

  <div class="tituloBlog">
    <h2>
       <a href="/blog" class="blanco">Blog</a>->{{ $posts->titulo }}
    </h2>
    </div>


  <div class="blanco margin">
  <span>Fecha de publicación:</span>
  {{$posts->created_at}}

  </div>

  <br>

  <div class="blanco margin" >
  <span>Autor:</span>
  {{$posts->user['name']}}
  </div>

  <div class="p blanco">

    <pre class="verdana">{{$posts->contenido}}</pre>
    <br><br><br>

  </div>

  <br><br>

</div>
<div id="app">
  <pie-component></pie-component>
</div>


@endsection