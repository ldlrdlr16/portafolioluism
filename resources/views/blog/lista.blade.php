@extends('layouts.app')

@section('titulo', 'Blog')

@section('content')





    <div class="tituloBlog">
        <h2><a href="/blog" class="blanco">Blog</a>->Lista de posts</h2>
    </div>
 
    <div class="row">

        <div class="columnaIzquierda">
            @foreach ($posts as $post)
            <a href="/blog/{{$post->id}}" class="blanco">
                <div class="tarjeta">
                    <h2>{{$post->titulo}}</h2>
                    <h4>Autor: {{$post->user['name']}}</h4>
                    <h5>{{$post->created_at}}</h5>
                    <p class="recortarTexto" style="width: 200px">{{$post->contenido}}</p>
                    <span>
                        <form method="POST" action="/blog/{{ $post->id }}" style="display: inline">
                            @method('DELETE')
                            @csrf
                            
                            <button type="submit" class="blanco link letrasMenu" onclick="return confirm('Estás seguro que deseas eliminar el registro?');">🗑️Borrar</button>
                        </form>
                    <a href="/blog/{{ $post->id }}/edit" class="blanco letrasMenu link">✏️Editar</a>

                    </span>
                </div>
            </a>
            @endforeach
        </div>

 
 
        <div class="columnaDerecha">
                <a href="/blog/create" class="tarjeta">
                    <h2 class="center blanco">Redactar un post</h2>
                </a>
                <a href="lista" class="tarjeta">
                    <h2 class="center blanco">Lista de posts</h2>
                </a>
            <div class="tarjeta">
                <h2>Sobre mi</h2>
                <p>Hola me llamo luis, tengo 22 años, estudio informatica en el IUJO y mi pais natal es Venezuela</p>
            </div>

        </div>

  
    </div>

    <div id="app">
        <pie-component></pie-component>
    </div>




@endsection

