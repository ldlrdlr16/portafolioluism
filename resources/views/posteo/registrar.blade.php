@extends('layouts.app')

@section('titulo', 'Registrar')

@section('content')


    <div class="registrar">
        <a href="/posts" class="blanco verdana">Lista</a>
    </div>


    
<br>


    <form method="post" action="/posts" id="registrar" class="formularioRegisPost">
    @csrf
            <h1>Registrar</h1>

            <label>Titulo</label>
            <input type="text" id="titulo" name="titulo" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,67)" onkeydown="return limitar(event,this.value,67)" class="inputt" autofocus autocomplete="off" value=" {{old('titulo')}} "/>

            <br>
            <label>Contenido</label>
            <textarea  id="contenido" class="verdana" name="contenido" cols="110" rows="50" onkeypress="return noSimbolosContenido(event)" onkeyup="return limitar(event,this.value,5000)" onkeydown="return limitar(event,this.value,5000)" class="inputt" autocomplete="off" value=" {{old('contenido')}} "></textarea>

            <br>
            <button type="submit" name="registrar" value="registrar">Confirmar</button>
            <br>
    </form>
    <div id="app">
        <pie-component></pie-component>
    </div>
  
    <script src="{{asset('js/posteo/val_regi.js')}}"></script>

    

@endsection