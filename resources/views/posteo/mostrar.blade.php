@extends('layouts.app')

@section('titulo', 'Mostrar')

@section('content')

<div class="registrar">
  <a href="/posts" class="blanco verdana">Lista</a>
  </div>
  
  <br>

<div class="mostrar">

  <br>

  <h1 class="hone">{{ $posts->titulo }}</h1>

  <div class="blanco margin">
  <span>Fecha de publicación:</span>
  {{$posts->created_at}}

  </div>

  <br>

  <div class="blanco margin" >
  <span>Autor:</span>
  {{$posts->user['name']}}
  </div>

  <div class="p blanco">

    <pre class="verdana">{{$posts['contenido']}}</pre>
    <br><br><br>

  </div>

  <br><br>

</div>
<div id="app">
  <pie-component></pie-component>
</div>


@endsection