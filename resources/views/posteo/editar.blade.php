@extends('layouts.app')

@section('titulo', 'Editar')

@section('content')

    <div class="registrar">
        <a href="/posts" class="blanco verdana">Ver Lista</a>
    </div>



    <form method="post" action="/posts/{{ $post->id }}" id="editar" class="formulario">
      @method('PATCH')
      @csrf
              <h1>Editar</h1>
   
              <label>Titulo</label>
              <input type="text" id="titulo" name="titulo" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,67)" onkeydown="return limitar(event,this.value,67)" class="inputt" autofocus autocomplete="off" value="{{ $post->titulo}}"/>
  
              <label>Contenido</label>
              <textarea  id="contenido" class="verdana" name="contenido" cols="80" rows="28" onkeypress="return noSimbolosContenido(event)" onkeyup="return limitar(event,this.value,1300)" onkeydown="return limitar(event,this.value,1300)" class="inputt" autocomplete="off">{{ $post->contenido }}</textarea>
  
              <br>
              <button type="submit" name="registrar" value="registrar">Confirmar</button>
              <br>
      </form>

<div id="app">
  <pie-component></pie-component>
</div>

<script src="{{asset('js/posteo/val_edi.js')}}"></script>


@endsection