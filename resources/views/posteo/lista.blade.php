@extends('layouts.app')

@section('titulo', 'Lista')

@section('content')


<div class="registrar">
<a href="/posts/create" class="blanco verdana">Registrar</a>
</div>

<br>

<div class="plista">
  <h1 class="blanco">Lista de Investigaciones</h1>
  <ul class="">
    @foreach ($posts as $post)
      <li class="">
        <h2 class="blanco">
          <div class="recortarTexto">
          <a href="/posts/{{$post['id']}}" class="blanco"> {{ $post->titulo }} </a>
          </div>


          <form method="POST" action="/posts/{{ $post->id }}" style="display: inline">
            @method('DELETE')
            @csrf
            
            <button type="submit" class="blanco link letrasMenu" onclick="return confirm('Estás seguro que deseas eliminar el registro?');">🗑️Borrar</button>
          </form>

          <a href="/posts/{{ $post['id'] }}/edit" class="blanco letrasMenu">✏️Editar</a>
 
        </h2>
      </li>
    @endforeach
  </ul>


 


</div>
<div id="app">
  <pie-component></pie-component>
</div>



@endsection