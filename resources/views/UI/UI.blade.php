@extends('layouts.app')

@section('titulo', 'UI')

@section('content')


    <div class="UI">

        <br>

        <h1 class="h">Diseño UI</h1>


        <div class="p">
            

            <p>El término UI viene de user interface, este se refiere a la experiencia del usuario cuando este navega por la página, el usuario no debe poner esfuerzos en buscar el cómo hacer ciertas acciones en la página web, la página debe mostrarle facilmente al usuario en donde se debe interactuar para lograr esas acciones.</p>
            <p>En el momento que una persona entra en tu sitio web, existen algunas acciones específicas que deseas que esta tome.</p>
            <p>Un buen diseño de UI permitira guiar a los usuarios por la navegación y los llevará a tomar dichas acciones de manera espontánea. </p>
            <p>La interfaz abarca desde temas en diseño web, como la creación de elementos que son visibles para el usuario a través de los que puede efectuar alguna acción, como llenar un formulario de pedido, al mismo tiempo, UI tiene que brindar una excelente experiencia de usuario, sobre todo si la plataforma digital está enfocada a un negocio de e-commerce.</p>
            <p>Los usuarios deben experimentar un sistema homogéneo, en donde el lenguaje, uso de colores y elementos gráficos sean consistentes. Si pruebas con diversos colores, tipografías e imágenes para cada pestaña de tu página web, podrías confundir al usuario y lo que él busca es simplicidad.</p>
            
            <br><br>
            <img src="{{ asset('img/ui-ux.jpg') }}" alt="" class="uiImg">


        </div>

        <br><br>



    </div>   

    <div id="app">
        <pie-component></pie-component>
    </div>

@endsection