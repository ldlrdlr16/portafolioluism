@extends('layouts.app')

@section('titulo', 'Calculadorav2')

@section('content')

    <div class="calcu">

    <br>
    
    <form action="calculadorav2" name="form" method="post" autocomplete="off" id="formulario">
    @csrf
        <label for="" class="labels">Primer digito</label>

        <input type="number" id="val1" name="val1" class="configInputs inputt" autofocus onkeypress="return soloNumeros(event)" value="@if(isset($val1)){{$val1}}@endif">
        
        <br><br>

        <label for="" class="labels">Segundo digito</label>

        <input type="number" id="val2" name="val2" class="configInputs inputt" onkeypress="return soloNumeros(event)" value="@if(isset($val2)){{$val2}}@endif">
        
        <br><br>
        
        <button name="boton" id="sumar" value="1" class="boton botonMargin">+</button>
        <button name="boton" id="restar" value="2" class="boton">-</button>
        <button name="boton" id="multiplicar" value="3" class="boton">*</button>
        <button name="boton" id="dividir" value="4" class="boton">/</button>


    </form>
    <br>

    @if (isset($resultado))
        <p class="verdana blanco">resultado: {{ $resultado }}</p>
    @endif

    @if ($errors->any())
        <ul class="blanco verdana" >
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    
    <br><br>

</div>
<div id="app">
    <piea-component></piea-component>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{asset('js/calculadorav2/calcu.js')}}"></script>

@endsection