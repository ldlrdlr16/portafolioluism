@extends('layouts.app')

@section('titulo', 'Editar')

@section('content')

    <div class="registrar">
        <a href="/curriculum" class="blanco verdana">Lista de Curriculums</a>
    </div>


    
<br>


    <form method="POST" action="/curriculum/{{$post->id}}" id="registrar" class="formularioRegisPost">
        @method('patch')
    @csrf
            <h1><a href="/curriculum" class="blanco">Curriculum</a>->Editar</h1>

            <label>Puesto</label>
            <input type="text" id="titulo" name="titulo" onkeypress="return noSimbolosN(event)" onkeyup="return limitar(event,this.value,50)" onkeydown="return limitar(event,this.value,50)" class="inputt" autofocus autocomplete="off" value="{{$post->titulo}}"/>
            
            <label>Nombre</label>
            <input type="text" id="nombre" name="nombre" onkeypress="return noSimbolosN(event)" onkeyup="return limitar(event,this.value,25)" onkeydown="return limitar(event,this.value,25)" class="inputt"  autocomplete="off" value="{{$post->nombre}}"/>

            <label>Apellido</label>
            <input type="text" id="apellido" name="apellido" onkeypress="return noSimbolosN(event)" onkeyup="return limitar(event,this.value,25)" onkeydown="return limitar(event,this.value,25)" class="inputt"  autocomplete="off" value="{{$post->apellido}}"/>

            <label>Correo</label>
            <input type="email" id="correo" name="correo" onkeypress="return noSimbolosCorreo(event)" onkeyup="return limitar(event,this.value,38)" onkeydown="return limitar(event,this.value,38)" class="inputt"  autocomplete="off" value="{{$post->correo}}"/>

            <label>Ubicacion</label>
            <input type="text" id="ubicacion" name="ubicacion" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,50)" onkeydown="return limitar(event,this.value,50)" class="inputt"  autocomplete="off" value="{{$post->ubicacion}}"/>

            <label>Telefono</label>
            <input type="text" id="telefono" name="telefono" onkeypress="return numeros(event)" onkeyup="return limitar(event,this.value,11)" onkeydown="return limitar(event,this.value,11)" class="inputt"  autocomplete="off" value="{{$post->telefono}}"/>

 
            <br><br>
            <label>Habilidades</label>
            <input type="text" id="skill1" name="skills[]" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autocomplete="off" value="{{$post->skill1}}"/>
            <input type="text" id="skill2" name="skills[]" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autocomplete="off" value="{{$post->skill2}}"/>
            <input type="text" id="skill3" name="skills[]" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autocomplete="off" value="{{$post->skill3}}"/>
            <input type="text" id="skill4" name="skills[]" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autocomplete="off" value="{{$post->skill4}}"/>
            <input type="text" id="skill5" name="skills[]" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autocomplete="off" value="{{$post->skill5}}"/>
            <input type="text" id="skill6" name="skills[]" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autocomplete="off" value="{{$post->skill6}}"/>

            <br><br>
            <label>Tecnologias</label>
            <input type="text" id="tecnologia1" name="tecnologi[]" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autocomplete="off" value="{{$post->tecnologia1}}"/>
            <input type="text" id="tecnologia2" name="tecnologi[]" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autocomplete="off" value="{{$post->tecnologia2}}"/>
            <input type="text" id="tecnologia3" name="tecnologi[]" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autocomplete="off" value="{{$post->tecnologia3}}"/>
            <input type="text" id="tecnologia4" name="tecnologi[]" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autocomplete="off" value="{{$post->tecnologia4}}"/>
            <input type="text" id="tecnologia5" name="tecnologi[]" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autocomplete="off" value="{{$post->tecnologia5}}"/>
            <input type="text" id="tecnologia6" name="tecnologi[]" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,30)" onkeydown="return limitar(event,this.value,30)" class="inputt" autocomplete="off" value="{{$post->tecnologia6}}"/>
            

            <br>

            <label>Experiencia</label>
            <textarea  id="contenido" class="verdana" name="contenido" cols="110" rows="50" onkeypress="return noSimbolosContenido(event)" onkeyup="return limitar(event,this.value,5000)" onkeydown="return limitar(event,this.value,5000)" class="inputt" autocomplete="off">{{$post->contenido}}</textarea>

            <br>
            <button type="submit" name="registrar" value="registrar">Confirmar</button>
            <br>
    </form>
    <div id="app">
        <pie-component></pie-component>
    </div>


    <script src="{{asset('js/curriculume.js')}}"></script>

    

@endsection