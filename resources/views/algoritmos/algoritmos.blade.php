@extends('layouts.app')

@section('titulo', 'Algoritmos de Encriptación')

@section('content')


    <div class="UI">

        <br>

        <h1 class="h">Algoritmos de Encriptación</h1>

        <div class="p">

            <p> Un algoritmo de encriptación es básicamente una función matemática que contiene datos de entrada junto con una clave. Si la función se calcula con la clave correcta, se generan datos cifrados como formato de salida. Si el mensaje debe ser decodificado, debe ser devuelto a su forma original con una llave.</p>
            <p> Dependiendo del proceso de encriptación, existen numerosos algoritmos, claves y modelos para la verificación de transmisores y receptores. Los métodos de encriptación incluyen no sólo uno o más algoritmos, sino también principios clave de distribución y verificación de la autenticidad e integridad de los datos y comunicación de los participantes.</p>
            <h3> Ejemplo Hash</h3>
            <p> Una función criptográfica hash- usualmente conocida como “hash”- es un algoritmo matemático que transforma cualquier bloque arbitrario de datos en una nueva serie de caracteres con una longitud fija. Independientemente de la longitud de los datos de entrada, el valor hash de salida tendrá siempre la misma longitud.</p>
            <br><br>

        </div>

        <br><br>

    </div>   

    <div id="app">
        <pie-component></pie-component>
    </div>

@endsection