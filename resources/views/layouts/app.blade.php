<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script src="{{asset('js/app.js')}}"></script>
    
    <title>@yield('titulo')</title>
    <link href="{{asset('css/css.css')}}" rel="stylesheet">

    
</head>
<body>
    
    <div class="menu" >
        @guest
        <a href="/" class="luis"> LUIS </a>
        @else
        <a href="/" class="luis"> {{ Auth::user()->name }} </a>
        @endguest
    
        
        @guest

        <a href="{{ route('register') }}" class="letrasMenu finalMenu">Registrarse</a>

        @else

        <a href="{{ route('logout') }}" 
        onclick="event.preventDefault(); document.getElementById('logout-form').submit();" 
        class="letrasMenu finalMenu">Salir</a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="nover">
            @csrf
        </form>



        @endguest

        

        @guest

        <a href="{{ route('login') }}" class="letrasMenu">Iniciar Sesion</a>
            
        @endguest
        
        <a href="/" class="letrasMenu">Inicio</a>

    </div>

    <br><br>



@yield('content')

<script src="{{asset('js/app.js')}}"></script>

</body>
</html>