@extends('layouts.app')

@section('titulo', 'Registrarse')

@section('content')


    <form method="POST" action="{{ route('register') }}" id="regi" class="formulario">
            @csrf

            <h1>Registrarse</h1>
            
            <br>

            <label>Usuario</label>
            <input type="text"     id="nombreUsuario" name="name" @error('name') is-invalid @enderror value="{{ old('name') }}" autocomplete="name" onkeypress="return noSimbolos(event)" onkeyup="return limitar(event,this.value,20)" onkeydown="return limitar(event,this.value,20)" class="inputt" autofocus autocomplete="off"/>

            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <br>
 
            <label>Correo</label>
            <input type="email"    id="correo"    @error('email') is-invalid @enderror value="{{ old('email') }}" required autocomplete="email"     name="email" onkeypress="return noSimbolosCorreo(event)" onkeyup="return limitar(event,this.value,38)" onkeydown="return limitar(event,this.value,38)" class="inputt"  autocomplete="off"/>

            @error('email')
                <span class="invalid-feedback" role="alert" style="color: red">
                    <strong>{{ "El correo ya ha sido registrado" }}</strong>
                </span>
            @enderror
            <br>

            <label>Contraseña</label>
            <input type="password" id="clave"         name="password"    @error('password') is-invalid @enderror autocomplete="new-password"     onkeypress="return noSimbolosClave(event)" onkeyup="return limitar(event,this.value,10)" onkeydown="return limitar(event,this.value,10)" class="inputt"  autocomplete="off"/>

            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <br>

            <label>Verificar Contraseña</label>
            <input type="password" id="clave2"        name="password_confirmation"  autocomplete="new-password"      onkeypress="return noSimbolosClave(event)" onkeyup="return limitar(event,this.value,10)" onkeydown="return limitar(event,this.value,10)" class="inputt"  autocomplete="off"/>

            <br>

            <button type="submit" id="enviar" name="registrarse" value="registrarse">Confirmar</button>





            <br>
    </form>
    <div id="app">
        <piea-component></piea-component>
    </div>


    <script src="{{asset('js/iniciarSesion/val_regi.js')}}"></script>
    

@endsection