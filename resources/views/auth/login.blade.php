@extends('layouts.app')

@section('titulo', 'Iniciar Sesion')

@section('content')

    <form method="POST" action="{{ route('login') }}"  id="ini"  class="formulario" >
        @csrf
        
        <h1>Iniciar Sesión</h1>
         
            
        <br>
 
        <label>Correo</label>
        <input type="text"    id="correo"  @error('email') is-invalid @enderror      name="email"  onkeypress="return noSimbolosCorreo(event)"      onkeyup="return limitar(event,this.value,38)" onkeydown="return limitar(event,this.value,38)" class="inputt"  autocomplete="email" value="{{ old('email') }}" autofocus/>

        @error('email')
        <span class="" role="alert" style="color: red">
            <strong>{{ 'El correo o la contraseña es incorrecto' }}</strong>
        </span>
        @enderror

        <br>

        <label>Contraseña</label>
        <input  type="password" id="clave" @error('password') is-invalid @enderror  name="password" class="inputt" onkeypress="return noSimbolosClave(event)" onkeyup="return limitar(event,this.value,10)" onkeydown="return limitar(event,this.value,10)" autocomplete="off"/>

        @error('password')
        <span class="blanco" role="alert" style="color: red">
            <strong>{{ 'Escriba alguna contraseña' }}</strong>
        </span>
        @enderror

        <br>


        <label>Recuerda mi sesión</label>
        <input type="checkbox" name="remember" id="">

        <br>
 
        <button type="submit"  id="enviar" name="iniciarSesion" value="iniciarSesion">Confirmar</button>

        <br>

        <br>
    </form>
    

    <div id="app">
        <piea-component></piea-component>
    </div>


    <script src="{{asset('js/iniciarSesion/val_ini.js')}}"></script>

@endsection