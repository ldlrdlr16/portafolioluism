document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("registrar").addEventListener("submit", validar);
  });
  
  
  function validar(e) {
  
  //Variables que contienen el valor de los inputs
  let titulo = document.getElementById('titulo').value;
  let contenido = document.getElementById('contenido').value;
  let nombre = document.getElementById('nombre').value;
  let apellido = document.getElementById('apellido').value;

  let correo  = document.getElementById('correo').value;
  let patronCorreo  = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

  let ubicacion = document.getElementById('ubicacion').value;
  let telefono = document.getElementById('telefono').value;

  let skill1 = document.getElementById("skill1").value;
  let skill2 = document.getElementById("skill2").value;
  let skill3 = document.getElementById("skill3").value;
  let skill4 = document.getElementById("skill4").value;
  let skill5 = document.getElementById("skill5").value;
  let skill6 = document.getElementById("skill6").value;

  let tecnologia1 = document.getElementById('tecnologia1').value;
  let tecnologia2 = document.getElementById('tecnologia2').value;
  let tecnologia3 = document.getElementById('tecnologia3').value;
  let tecnologia4 = document.getElementById('tecnologia4').value;
  let tecnologia5 = document.getElementById('tecnologia5').value;
  let tecnologia6 = document.getElementById('tecnologia6').value;


  if (titulo.length < 1) {
    e.preventDefault();
    alert('Debe escribir algo en titulo');
    document.getElementById("titulo").focus();
    return;
  }
  if (nombre.length < 1) {
    e.preventDefault();
    alert('Debe escribir algo en nombre');
    document.getElementById("contenido").focus();
    return;
  }
  if (apellido.length < 1) {
    e.preventDefault();
    alert('Debe escribir algo en apellido');
    document.getElementById("apellido").focus();
    return;
  }
  if (correo.length < 14 || correo > 38) {
    e.preventDefault();
    alert('Su correo debe contener entre 14 y 38 caracteres');
    document.getElementById("correo").focus();
    return;
  }
  if (!patronCorreo.test(correo)) {
    e.preventDefault();
    alert('Escriba el correo correctamente');
    document.getElementById("correo").focus();
    return;
  }
  if (ubicacion.length < 1) {
    e.preventDefault();
    alert('Debe escribir algo en ubicacion');
    document.getElementById("ubicacion").focus();
    return;
  }
  if (telefono.length < 1) {
    e.preventDefault();
    alert('Debe escribir algo en telefono');
    document.getElementById("telefono").focus();
    return;
  }
  if (skill6.length > 0 && skill5.length < 1) {
    e.preventDefault();
    alert('Debe escribir en correcto orden por favor');
    document.getElementById("skill5").focus();
    return;
  }
  if (skill5.length > 0 && skill4.length < 1) {
    e.preventDefault();
    alert('Debe escribir en correcto orden por favor');
    document.getElementById("skill4").focus();
    return;
  }
  if (skill4.length > 0 && skill3.length < 1) {
    e.preventDefault();
    alert('Debe escribir en correcto orden por favor');
    document.getElementById("skill3").focus();
    return;
  }
  if (skill3.length > 0 && skill2.length < 1) {
    e.preventDefault();
    alert('Debe escribir en correcto orden por favor');
    document.getElementById("skill2").focus();
    return;
  }
  if (skill2.length > 0 && skill1.length < 1) {
    e.preventDefault();
    alert('Debe escribir en correcto orden por favor');
    document.getElementById("skill1").focus();
    return;
  }
  if (skill1.length < 1) {
    e.preventDefault();
    alert('Escriba alguna habilidad');
    document.getElementById("skill1").focus();
    return;
  }



  if (tecnologia6.length > 0 && tecnologia5.length < 1) {
    e.preventDefault();
    alert('Debe escribir en correcto orden por favor');
    document.getElementById("tecnologia5").focus();
    return;
  }
  if (tecnologia5.length > 0 && tecnologia4.length < 1) {
    e.preventDefault();
    alert('Debe escribir en correcto orden por favor');
    document.getElementById("tecnologia4").focus();
    return;
  }
  if (tecnologia4.length > 0 && tecnologia3.length < 1) {
    e.preventDefault();
    alert('Debe escribir en correcto orden por favor');
    document.getElementById("tecnologia3").focus();
    return;
  }
  if (tecnologia3.length > 0 && tecnologia2.length < 1) {
    e.preventDefault();
    alert('Debe escribir en correcto orden por favor');
    document.getElementById("tecnologia2").focus();
    return;
  }
  if (tecnologia2.length > 0 && tecnologia1.length < 1) {
    e.preventDefault();
    alert('Debe escribir en correcto orden por favor');
    document.getElementById("tecnologia1").focus();
    return;
  }
  if (tecnologia1.length < 1) {
    e.preventDefault();
    alert('Escriba alguna habilidad');
    document.getElementById("tecnologia1").focus();
    return;
  }
  if (contenido.length < 1) {
    e.preventDefault();
    alert('Debe escribir algo en experiencia');
    document.getElementById("contenido").focus();
    return;
  }



  
  
  
  this.submit();
  }
  
  
  
      //Funcion para permitir al usuario ingresar solo letras y espacios.
      function noSimbolosN(e){
        let unicode=e.keyCode? e.keyCode : e.charCode;
      
        if(unicode==8 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40 || unicode==241)
          return true;
      
      
        patron= /[A-Za-z ]/;
        tecla_final = String.fromCharCode(unicode);
        return patron.test(tecla_final);
      }
  
  
  //Funcion para permitir al usuario ingresar solo números, letras y espacios.
  function noSimbolos(e){
    let unicode=e.keyCode? e.keyCode : e.charCode;
  
    if(unicode==8 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40 || unicode==241)
      return true;
  
  
    patron= /[A-Za-z0-9 ]/;
    tecla_final = String.fromCharCode(unicode);
    return patron.test(tecla_final);
  }

      //Funcion para permitir al usuario ingresar solo números.
      function numeros(e){
        let unicode=e.keyCode? e.keyCode : e.charCode;
      
        if(unicode==8 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40 )
          return true;
      
      
        patron= /[0-9]/;
        tecla_final = String.fromCharCode(unicode);
        return patron.test(tecla_final);
      }
  
  //Function para permitir al usuario ingresar solo números, letras, espacios y puntos.
  function noSimbolosContenido(e){
    let unicode=e.keyCode? e.keyCode : e.charCode;
  
    if(unicode==8 || unicode==46 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40 || unicode==44 || unicode == 241)
      return true;
  
  
    patron= /[A-Za-z0-9, ]/;
    tecla_final = String.fromCharCode(unicode);
    return patron.test(tecla_final);
  }
  
  
  
  //Lo mismo que noSimbolos pero sin espacio
  function noSimbolosClave(e){
    let unicode=e.keyCode? e.keyCode : e.charCode;
  
    if(unicode==8 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40)
      return true;
  
  
    patron= /[A-Za-z0-9]/;
    tecla_final = String.fromCharCode(unicode);
    return patron.test(tecla_final);
  }
  
  
  //Function para limitar el ingreso de caracteres en un input
  function limitar(e,valor,caracteres) {
  
    let unicode=e.keyCode? e.keyCode : e.charCode;
  
    // Permitimos las siguientes teclas:
    // 8 backspace
    // 46 suprimir
    // 13 enter
    // 9 tabulador
    // 37 izquierda
    // 39 derecha
    // 38 subir
    // 40 bajar
    //36 46
     if(unicode==8 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40)
      return true;
  
    // Si ha superado el limite de caracteres devolvemos false
    if (valor.length >= caracteres) {
      return false;
    }
  
    return true;
    
  }
  
  