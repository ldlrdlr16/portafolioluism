document.addEventListener("DOMContentLoaded", function() {
  document.getElementById("formulario").addEventListener("submit", validar); 
});


function validar(e) {

//Variables que contienen el valor de los inputs
let val1 = document.getElementById('val1').value;
let val2   = document.getElementById('val2').value;
let opc   = document.getElementById('opcion').value;



if(val1.length < 1) {
  e.preventDefault();
  alert('No debe dejar el input vacio');
  document.getElementById("val1").focus();
  return;
}
if (val2.length < 1) {
  e.preventDefault();
  alert('No debe dejar el input vacio');
  document.getElementById("val2").focus();
  return;
}

if ((val2 == 0) && (opc == 4)) {
  e.preventDefault();
  alert('No puede dividir entre 0');
  document.getElementById("val2").focus();
  return;
}


this.submit();
}

//Function para permitir al usuario ingresar solo números.
function soloNumeros(e){
    let unicode=e.keyCode? e.keyCode : e.charCode;
  
  
    patron= /[0-9]/;
    tecla_final = String.fromCharCode(unicode);
    return patron.test(tecla_final);
  }