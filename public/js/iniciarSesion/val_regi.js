document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("regi").addEventListener("submit", validar); 
});

 
function validar(e) {

  //Variables que contienen el valor de los inputs
  let usuario = document.getElementById('nombreUsuario').value;
  let clave   = document.getElementById('clave').value;
  let clave2  = document.getElementById('clave2').value;
  let correo  = document.getElementById('correo').value;
  let patronCorreo  = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
  let patronUsuario = /^(?=.*[A-Za-z])(?=.*)[A-Za-z\d]{4,}$/;



  if(usuario.length < 4 || usuario.length > 20) {
    e.preventDefault();
    alert('Su usuario debe contener entre 4 a 20 caracteres');
    document.getElementById("nombreUsuario").focus();
    return;
  }

  if (!patronUsuario.test(usuario)) {
    e.preventDefault();
    alert('El usuario debe contener al menos una letra');
    document.getElementById("nombreUsuario").focus();
    return;
  }
  
  if (correo.length < 14 || correo > 38) {
    e.preventDefault();
    alert('Su correo debe contener entre 14 y 38 caracteres');
    document.getElementById("correo").focus();
    return;
  }

  if (!patronCorreo.test(correo)) {
    e.preventDefault();
    alert('Escriba el correo correctamente');
    document.getElementById("correo").focus();
    return;
  }

  if (clave.length < 8 || clave.length > 10) {
    e.preventDefault();
    alert('Su contraseña debe contener entre 8 a 10 caracteres');
    document.getElementById("clave").focus();
    return;
  }

  if(clave != clave2){
    e.preventDefault();
    alert('Las contraseñas no coinciden, introduzca las contraseñas correctamente');
    document.getElementById("clave2").focus();
    return;
  }


  this.submit();
}


//Function para permitir al usuario ingresar solo números, letras y espacios.
function noSimbolos(e){
  let unicode=e.keyCode? e.keyCode : e.charCode;

  if(unicode==8 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40)
  {
    return true;
  }


  patron= /[A-Za-z0-9]/;
  tecla_final = String.fromCharCode(unicode);
  return patron.test(tecla_final);
}

//Funcion para permitir al usuario ingresar solo numeros, letras y puntos
function noSimbolosCorreo(e){
  let unicode=e.keyCode? e.keyCode : e.charCode;

  if(unicode==8 || unicode==46 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40 || unicode==64)
  {
    return true;
  }

  patron= /[A-Za-z0-9]/;
  tecla_final = String.fromCharCode(unicode);
  return patron.test(tecla_final);
}

//Lo mismo que noSimbolos pero sin espacio
function noSimbolosClave(e){
  let unicode=e.keyCode? e.keyCode : e.charCode;

  if(unicode==8 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40)
    return true;


  patron= /[A-Za-z0-9]/;
  tecla_final = String.fromCharCode(unicode);
  return patron.test(tecla_final);
}


//Function para limitar el ingreso de caracteres en un input
function limitar(e,valor,caracteres) {

  let unicode=e.keyCode? e.keyCode : e.charCode;

  // Permitimos las siguientes teclas:
  // 8 backspace
  // 46 suprimir
  // 13 enter
  // 9 tabulador
  // 37 izquierda
  // 39 derecha
  // 38 subir
  // 40 bajar
  if(unicode==8 || unicode==13 || unicode==9 || unicode==37 || unicode==38 || unicode==40)
    return true;

  // Si ha superado el limite de caracteres devolvemos false
  if (valor.length >= caracteres) {
    return false;
  }

  return true;
  
}