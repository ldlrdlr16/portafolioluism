<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Ruta de la pagina de inicio
Route::get('/', function () {
    return view('welcome');
});

//Ruta de la pagina-investigacion UI
Route::get('/UI', function () {
    return view('UI/UI');
});

//Ruta de la pagina-investigacion UX
Route::get('/UX', function () {
    return view('UX/UX');
});

//Ruta de la pagina-investigacion POO
Route::get('/POO', function () {
    return view('POO/POO');
});

//Ruta de la pagina-investigacion Buenas Practicas
Route::get('/buenasPracticas', function () {
    return view('buenasPracticas/buenasPracticas');
});

//Ruta de la pagina-investigacion Buenas Practicas
Route::get('/algoritmos', function () {
    return view('algoritmos/algoritmos');
});

//Pagina de prueba para la autenticacion requerida
route::view('/protegido', 'protegido/protegido')->middleware('auth');

//Rutas para la calculadorav1
Route::get('calculadorav1', [App\Http\Controllers\controladorCalculadora::class, 'index']);
Route::post('calculadorav1', [App\Http\Controllers\controladorCalculadora::class, 'calcular']);

//Rutas para la calculadora v2
Route::get('calculadorav2', [App\Http\Controllers\controladorCalculadorav2::class, 'index']);
Route::post('calculadorav2', [App\Http\Controllers\controladorCalculadorav2::class, 'calcular']);

//Rutas para registrar las investigaciones
Route::resource('posts', 'App\Http\Controllers\PostsController')->middleware('auth');

//Rutas para el inicio y registro de usuarios
Auth::routes();

//Pagina de bienvenida
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Rutas para el funcionamiento del blog
Route::resource('blog', 'App\Http\Controllers\blogControlador')->middleware('auth');

//Ruta con la lista de curriculums
Route::get('lista', [App\Http\Controllers\blogControlador::class, 'lista'])->middleware('auth');

//Rutas para los curriculum
Route::resource('curriculum', 'App\Http\Controllers\curriculumControlador')->middleware('auth');