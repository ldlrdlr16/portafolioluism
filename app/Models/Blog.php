<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    public function user(){
        //Metodo que crea una relacion de uno a muchos para los posts que cree el usuario
        return $this->belongsTo('app\Models\User', 'userId');

    }
}
