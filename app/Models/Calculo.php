<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

    //Clase que posee el metodo para resolver las operaciones
class Calculo extends Model
{
    use HasFactory;

    const
    sumar = 1,
    restar = 2,
    multiplicar = 3,
    dividir = 4;

    public function obtenerDatos(): array
    {
        return [
            self::sumar => 'sumar',
            self::restar => 'restar',
            self::multiplicar => 'multiplicar',
            self::dividir => 'dividir',
        ];
    }

    //Metodo para las operaciones
    public function calcularDatos(int $val1, int $val2, int $opcion): ?int
    {

        switch ($opcion)
        {
            case self::sumar:
                return $val1+$val2;
            break;


            case self::restar:
                return $val1-$val2;
            break;


            case self::multiplicar:
                return $val1*$val2;
            break;


            case self::dividir:
                return $val1/$val2;
            break; 


            default:
                return null;
            break;
        }

    }    
}

