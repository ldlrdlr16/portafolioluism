<?php

namespace App\Http\Controllers;

use App\Models\Curriculum;
use Illuminate\Http\Request;

class curriculumControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Metodo para mostrar la pagina de inicio de curriculum y pedir a la base de datos los registros

        $posts= Curriculum::orderBy('created_at', 'desc')
        //->take(0) Cantidad de elementos obtenidos de la bd
        ->get();
                    
        return view('curriculum.curriculum', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Metodo que muestra la vista que registra los curriculos
        return view('curriculum.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Metodo que se encarga de guardar el registro en la base de datos
        $post = new Curriculum();

        $post->titulo = request('titulo');
        $post->contenido = request('contenido');
        $post->nombre = request('nombre');  
        $post->apellido = request('apellido');

        $post->correo = request('correo');
        $post->ubicacion = request('ubicacion');
        $post->telefono = request('telefono');

        $habilidades = request('skills');

        $tecnologi = request('tecnologi');


        if(isset($habilidades[0]))
        $post->skill1=$habilidades[0];
        if(isset($habilidades[1]))
        $post->skill2=$habilidades[1];
        if(isset($habilidades[2]))
        $post->skill3=$habilidades[2];
        if(isset($habilidades[3]))
        $post->skill4=$habilidades[3];
        if(isset($habilidades[4]))
        $post->skill5=$habilidades[4];
        if(isset($habilidades[5]))
        $post->skill6=$habilidades[5];

        if(isset($tecnologi[0]))
        $post->tecnologia1=$tecnologi[0];
        if(isset($tecnologi[1]))
        $post->tecnologia2=$tecnologi[1];
        if(isset($tecnologi[2]))
        $post->tecnologia3=$tecnologi[2];
        if(isset($tecnologi[3]))
        $post->tecnologia4=$tecnologi[3];
        if(isset($tecnologi[4]))
        $post->tecnologia5=$tecnologi[4];
        if(isset($tecnologi[5]))
        $post->tecnologia6=$tecnologi[5];


        $post->save();

        return redirect('curriculum');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Curriculum  $curriculum
     * @return \Illuminate\Http\Response
     */
    public function show(Curriculum $post, $id, Request $request)
    {
        //Metodo que muestra un registro especificado por el usuario
        $post = Curriculum::find($id);


        return view('curriculum.mostrar', ['posts' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Curriculum  $curriculum
     * @return \Illuminate\Http\Response
     */
    public function edit(Curriculum $post, $id)
    {
        //Metodo que muestra la vista para editar un curriculo
        $post = Curriculum::find($id);

        return view('curriculum.editar', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Curriculum  $curriculum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Curriculum $post, $id)
    {
        //Metodo que actualiza el registro especificado por el usuario
        $post = Curriculum::findOrFail($id);

        $post->titulo = request('titulo');
        $post->contenido = request('contenido');
        $post->nombre = request('nombre');  
        $post->apellido = request('apellido'); 

        $post->correo = request('correo');
        $post->ubicacion = request('ubicacion');
        $post->telefono = request('telefono');

        $habilidades = request('skills');

        $tecnologi = request('tecnologi');

        if(isset($habilidades[0]) || $habilidades[0] == null )
        $post->skill1=$habilidades[0];
        if(isset($habilidades[1]) || $habilidades[1] == null )
        $post->skill2=$habilidades[1];
        if(isset($habilidades[2]) || $habilidades[2] == null )
        $post->skill3=$habilidades[2];
        if(isset($habilidades[3]) || $habilidades[3] == null )
        $post->skill4=$habilidades[3];
        if(isset($habilidades[4]) || $habilidades[4] == null )
        $post->skill5=$habilidades[4];
        if(isset($habilidades[5]) || $habilidades[5] == null )
        $post->skill6=$habilidades[5];

        if(isset($tecnologi[0]) || $tecnologi[0] == null  )
        $post->tecnologia1=$tecnologi[0];
        if(isset($tecnologi[1]) || $tecnologi[1] == null  )
        $post->tecnologia2=$tecnologi[1];
        if(isset($tecnologi[2]) || $tecnologi[2] == null  )
        $post->tecnologia3=$tecnologi[2];
        if(isset($tecnologi[3]) || $tecnologi[3] == null  )
        $post->tecnologia4=$tecnologi[3];
        if(isset($tecnologi[4]) || $tecnologi[4] == null  )
        $post->tecnologia5=$tecnologi[4];
        if(isset($tecnologi[5]) || $tecnologi[5] == null  )
        $post->tecnologia6=$tecnologi[5];

     
     


        $post->save();

        return redirect('curriculum');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Curriculum  $curriculum
     * @return \Illuminate\Http\Response
     */
    public function destroy(Curriculum $post, $id)
    {
        //Metodo que elimina el registro
        $post = Curriculum::find($id);

        $post->delete();

        return redirect('curriculum');
    }
}
