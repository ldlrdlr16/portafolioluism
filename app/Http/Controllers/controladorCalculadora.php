<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

use App\Models\Calculo;

class controladorCalculadora extends Controller
{
    public function index(Calculo $calculo){
        //Metodo que muestra la pagina inicio de la calculadora
        //Y crea una instancia del metodo obtenerDatos de la clase Calculo

        return view('calculadora/calculadora', [
            'operaciones' => $calculo->obtenerDatos()
        ]);

    }


    public function calcular(Calculo $calculo){
        //Metodo que extrae de la vista los valores introducidos por el usuario
        //Y hace la respectiva operacion
        //Tambien crea una instancia del metodo calcularDatos de la clase Calculo
        $this->validate(request(), [
            'val1' => ['required', 'integer'],
            'val2' => ['required', 'integer'],
            'opcion' => [
                'required',
                Rule::in(array_keys($calculo->obtenerDatos()))
            ]
        ]);

        $val1 = request()->input('val1');
        $val2 = request()->input('val2');
        $operacion = request()->input('opcion');
        $resultado = $calculo->calcularDatos($val1,$val2,$operacion);

        return view('calculadora/calculadora', [
            'operaciones' => $calculo->obtenerDatos(),
            'resultado' => $resultado,
            'val1' => $val1,
            'val2' => $val2
        ]);

    }
}
