<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
 
class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Metodo que muestra la vista de inicio y hace una peticion de los registros a la base de datos
        $posts= Post::orderBy('created_at', 'desc')
        //->take(0) Cantidad de elementos obtenidos de la bd
        ->get();
                    
        return view('posteo.lista', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Metodo que muestra la vista para registrar un nuevo registro
        return view('posteo.registrar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Metodo que guarda el nuevo registro en la base de datos
        $user = auth()->user();

        $post = new Post();

        $post->titulo = request('titulo');
        $post->contenido = request('contenido');
        $post->userId = $user->id;

        $post->save();

        return redirect('posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //Metodo que muestra el registro especificado por el usuario

 

        return view('posteo.mostrar', ['posts' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //Metodo que muestra la vista del registro que editara el usuario
        $post = Post::find($post->id);

        return view('posteo.editar', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //Metodo que actualiza el registro 
        $post = Post::findOrFail($post->id);

        $post->titulo = request('titulo');
        $post->contenido = request('contenido');

        $post->save();

        return redirect('posts');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //Metodo que elimina el registro
        $post = Post::find($post->id);

        $post->delete();

        return redirect('/posts');
    }
}
