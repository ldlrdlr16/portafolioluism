<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class blogControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Pagina de inicio del blog y peticion de los registros a la base de datos
        $posts= Blog::orderBy('created_at', 'desc')
        ->take(2) //Cantidad de elementos obtenidos de la bd
        ->get();

        return view('blog.blog', ['posts' => $posts]);
    }

    public function lista()
    {
        //Metodo que muestra la lista de posts del blog
        $posts= Blog::orderBy('created_at', 'desc')
        //->take(2) //Cantidad de elementos obtenidos de la bd
        ->get();
        return view('blog.lista', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Metodo que muestra la vista para registrar un blog
        return view('blog.registrar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Metodo que registra al nuevo usuario en la base de datos
        $user = auth()->user();

        $post = new Blog();

        $post->titulo = request('titulo');
        $post->contenido = request('contenido');
        $post->userId = $user->id;

        $post->save();

        return redirect('blog');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $post, Request $request, $id)
    {
        //Metodo que muestra un registro de la base de datos
        $post = Blog::find($id);

        return view('blog.mostrar', ['posts' => $post]);
    } 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $post, Request $request, $id)
    {
        //Metodo que muestra un registro de la base de datos para editarlo
        $post = Blog::find($id);

        return view('blog.editar', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $post, $id)
    {
        //Metodo que actualiza el registro de la base de datos
        $post = Blog::findOrFail($id);

        $post->titulo = request('titulo');
        $post->contenido = request('contenido');

        $post->save();

        return redirect('lista');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $post, $id)
    {
        //Metodo que borra un registro de la base de datos
        $post = Blog::find($id);

        $post->delete();

        return redirect('lista');
    }
}
