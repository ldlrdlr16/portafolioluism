<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //Metodo que da la bienvenida al usuario despues del registro o login
        //Nota: Actualmente no esta siendo usado y solo redirige a la pagina de inicio
        return view('welcome');
    }
}
